/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Dialog

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.app.FragmentManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import tracmo.tw.com.stationmanager.Entity.DHCP
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Entity.StaticIP
import tracmo.tw.com.stationmanager.Event.SetWifiEvent
import tracmo.tw.com.stationmanager.Listener.IDialogResultListener
import tracmo.tw.com.stationmanager.R


class DialogHelper{

    private val TAG = this.javaClass.simpleName

    private val Confirm_TAG = "$TAG:confirm"

    fun showConfirmDialog(fragmentManager: FragmentManager, message: String, listener: IDialogResultListener<Int>?, cancelable: Boolean, cancelListener: SettingParamDialog.OnDialogCancelListener) {
        val dialogFragment = SettingParamDialog().newInstance(object : SettingParamDialog.OnCallDialog {
            override fun getDialog(context: Context?): Dialog {
                val builder = AlertDialog.Builder(context, R.style.Base_AlertDialog)
                builder.setMessage(message)
                builder.setPositiveButton(R.string.ok, { dialog, which ->
                    listener?.onDataResult(which)
                })
//                builder.setNegativeButton(R.string.cancel, { dialog, which ->
//                    listener?.onDataResult(which)
//                })
                return builder.create()
            }
        }, cancelable, cancelListener)
        dialogFragment.show(fragmentManager, Confirm_TAG)

    }

    private val Params_TAG = "$TAG:params"

    /**
     * User set wifi detail in this dialog.
     *
     * SetWifiEvent,
     * {@link MainActivity#INDEX, MainActivity#NAME}
     *
     * DHCP mode:
     * {@link MainActivity#PASSWORD}
     *
     * Static IP mode:
     * {@link MainActivity#PASSWORD, MainActivity#IP_ADDRESS, MainActivity#SUBNET_MASK,
 *         MainActivity#ROUTER, MainActivity#DNS_1, MainActivity#DNS_2 }
     * */
    fun showWifiParamsDialog(manager: android.support.v4.app.FragmentManager, event: SetWifiEvent?, resultListener: IDialogResultListener<HashMap<String, String>>?, cancelable: Boolean) {
        val dialogFragment = SettingParamDialog().newInstance(object :SettingParamDialog.OnCallDialog {
            @SuppressLint("ResourceType")
            override fun getDialog(context: Context?): Dialog {

                val view = LayoutInflater.from(context).inflate(R.layout.dialog_wifi_params, null)

                val advance = view.findViewById(R.id.open) as ImageView
                val spinnerGroup = view.findViewById(R.id.spinnerGroup) as ConstraintLayout
                val staticIPGroup = view.findViewById(R.id.staticIPGroup) as ConstraintLayout
                val parameter = view.findViewById(R.id.parameter) as ConstraintLayout
                val settingSpinner = view.findViewById(R.id.ip_setting_spinner) as Spinner
                //spinner
                val itemArray = context!!.resources.getStringArray(R.array.ip_setting_array)
                val adapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, itemArray)
                settingSpinner.adapter = adapter
                settingSpinner.onItemSelectedListener = itemSelected(staticIPGroup)
                //advance click ui change
                advance.setOnClickListener {
                    advance.isSelected = !advance.isSelected
                    when(advance.isSelected){
                        true -> {
                            //show advance list
                            spinnerGroup.visibility = View.VISIBLE
                        }
                        false -> {
                            //if list show, close
                            when(spinnerGroup.visibility == View.VISIBLE){
                                true -> {
                                    settingSpinner.setSelection(0)
                                    spinnerGroup.visibility = View.GONE
                                    staticIPGroup.visibility = View.GONE
                                }
                            }
                        }
                    }
                }

                return when( event == null ){
                    true -> {
                        LogD(TAG, " event == null")
                        //edit title
                        noEvent(context,  view, parameter, spinnerGroup, settingSpinner, itemArray, resultListener).create()
                    }
                    false -> {
                        LogD(TAG, " event != null")
                        withEvent(context, event!!, view, parameter, spinnerGroup, settingSpinner, itemArray, resultListener).create()
                    }
                }
//                return builder.setCustomTitle(getTitle(event.name, context))
//                        .setView(view)
//                        .setPositiveButton(R.string.ok, { dialog, which ->
//
//                            val index = event.index.toString()
//                            val name = event.name
//                            val password = (view.findViewById(R.id.password) as EditText).text.toString()
//                            val ipAddress = (parameter.findViewById(R.id.ip_address) as EditText).text.toString()
//                            val subnetMask = (parameter.findViewById(R.id.subnet_mask) as EditText).text.toString()
//                            val router = (parameter.findViewById(R.id.router) as EditText).text.toString()
//                            val dns1 = (parameter.findViewById(R.id.dns1) as EditText).text.toString()
//                            val dns2 = (parameter.findViewById(R.id.dns2) as EditText).text.toString()
//
//                            when(!index.isEmpty() && !name.isEmpty() && !password.isEmpty()){
//                                true -> {
//                                    when(spinnerGroup.visibility){
//                                    //no advAnce, only dhcp
//                                        View.GONE -> resultListener!!.onDataResult(DHCP(index, password).mapData())
//
//                                    //advance, static ip
//                                        View.VISIBLE -> {
//                                            when(settingSpinner.selectedItem){
//                                            //dhcp
//                                                itemArray[0] -> {
//                                                    LogD(TAG, "${itemArray[0]} itemArray[0]")
//                                                    resultListener!!.onDataResult(DHCP(index, password).mapData())
//                                                }
//                                            //static ip
//                                                itemArray[1] -> {
//                                                    LogD(TAG, "${itemArray[1]} itemArray[1]")
//                                                    when(!ipAddress.isEmpty() && !subnetMask.isEmpty() && !router.isEmpty() && !dns1.isEmpty() && !dns2.isEmpty()){
//                                                        true -> resultListener!!.onDataResult(StaticIP(index, password, ipAddress, subnetMask, router, dns1, dns2).mapData())
//                                                        false -> resultListener!!.onDataResult(null)
//                                                    }
//
//                                                }
//                                            }
//
//                                        }
//
//                                    }
//                                }
//                                false -> resultListener!!.onDataResult(null)
//                            }
//
//                        }).setNegativeButton(R.string.cancel, null)
//                        .create()
            }
        }, cancelable, null)
        dialogFragment.show(manager, Params_TAG)
    }

    private fun getTitle(name: CharSequence, context: Context?): TextView{

        val title = TextView(context)
        title.text = name
        title.gravity = Gravity.CENTER
        title.textSize = 22.0f
        title.setPadding(5, 5, 10, 5)
        return title

    }

    private fun itemSelected(staticIPGroup: ConstraintLayout): AdapterView.OnItemSelectedListener{

        return object: AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                //dhcp
                    0 -> staticIPGroup.visibility = View.GONE
                //static ip
                    1 -> staticIPGroup.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) { }

        }
    }

    private fun withEvent(context: Context, event: SetWifiEvent, view: View, parameter: ConstraintLayout, spinnerGroup: ConstraintLayout,
                             settingSpinner: Spinner, itemArray: Array<String>, resultListener: IDialogResultListener<HashMap<String, String>>?): AlertDialog.Builder{
        val builder = AlertDialog.Builder(context, R.style.Base_AlertDialog)
        builder.setCustomTitle(getTitle(event.name, context))
                .setView(view)
                .setPositiveButton(R.string.ok) { dialog, which ->

                    val index = event.index.toString()
                    val password = (view.findViewById(R.id.password) as EditText).text.toString()
                    val ipAddress = (parameter.findViewById(R.id.ip_address) as EditText).text.toString()
                    val subnetMask = (parameter.findViewById(R.id.subnet_mask) as EditText).text.toString()
                    val router = (parameter.findViewById(R.id.router) as EditText).text.toString()
                    val dns1 = (parameter.findViewById(R.id.dns1) as EditText).text.toString()
                    val dns2 = (parameter.findViewById(R.id.dns2) as EditText).text.toString()

                    when(!index.isEmpty() && !password.isEmpty()){
                        true -> {
                            when(spinnerGroup.visibility){
                                //no advAnce, only dhcp
                                View.GONE -> resultListener!!.onDataResult(DHCP(index, null, password).mapData())

                                //advance, static ip
                                View.VISIBLE -> {
                                    when(settingSpinner.selectedItem){
                                        //dhcp
                                        itemArray[0] -> {
                                            LogD(TAG, "${itemArray[0]} itemArray[0]")
                                            resultListener!!.onDataResult(DHCP(index, null, password).mapData())
                                        }
                                        //static ip
                                        itemArray[1] -> {
                                            LogD(TAG, "${itemArray[1]} itemArray[1]")
                                            when(!ipAddress.isEmpty() && !subnetMask.isEmpty() && !router.isEmpty() && !dns1.isEmpty() && !dns2.isEmpty()){
                                                true -> resultListener!!.onDataResult(StaticIP(index, null, password, ipAddress, subnetMask, router, dns1, dns2).mapData())
                                                false -> resultListener!!.onDataResult(null)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        false -> resultListener!!.onDataResult(null)
                    }

                    dialog.dismiss()

                }.setNegativeButton (R.string.cancel) {dialog, which -> dialog.dismiss()}

        return builder
    }

    private fun noEvent(context: Context, view: View, parameter: ConstraintLayout, spinnerGroup: ConstraintLayout,
                             settingSpinner: Spinner, itemArray: Array<String>, resultListener: IDialogResultListener<HashMap<String, String>>?): AlertDialog.Builder {

        val nameGroup = view.findViewById(R.id.nameGroup) as ConstraintLayout
        nameGroup.visibility = View.VISIBLE
        val builder = AlertDialog.Builder(context, R.style.Base_AlertDialog)
        builder.setCustomTitle(getTitle(context.resources.getString(R.string.new_network), context))
                .setView(view)
                .setPositiveButton(R.string.ok) { dialog, which ->

                    val name = (view.findViewById(R.id.name) as EditText).text.toString()
                    val password = (view.findViewById(R.id.password) as EditText).text.toString()
                    val ipAddress = (parameter.findViewById(R.id.ip_address) as EditText).text.toString()
                    val subnetMask = (parameter.findViewById(R.id.subnet_mask) as EditText).text.toString()
                    val router = (parameter.findViewById(R.id.router) as EditText).text.toString()
                    val dns1 = (parameter.findViewById(R.id.dns1) as EditText).text.toString()
                    val dns2 = (parameter.findViewById(R.id.dns2) as EditText).text.toString()

                    when(!name.isEmpty() && !password.isEmpty()) {
                        true -> {
                            when(spinnerGroup.visibility){
                                //no advAnce, only dhcp
                                View.GONE -> resultListener!!.onDataResult(DHCP(null, name, password).mapNewData())

                                //advance, static ip
                                View.VISIBLE -> {
                                    when(settingSpinner.selectedItem){
                                        //dhcp
                                        itemArray[0] -> {
                                            LogD(TAG, "${itemArray[0]} itemArray[0]")
                                            resultListener!!.onDataResult(DHCP(null, name, password).mapNewData())
                                        }
                                        //static ip
                                        itemArray[1] -> {
                                            LogD(TAG, "${itemArray[1]} itemArray[1]")
                                            when(!ipAddress.isEmpty() && !subnetMask.isEmpty() && !router.isEmpty() && !dns1.isEmpty() && !dns2.isEmpty()){
                                                true -> resultListener!!.onDataResult(StaticIP(null, name, password, ipAddress, subnetMask, router, dns1, dns2).mapNewData())
                                                false -> resultListener!!.onDataResult(null)
                                            }

                                        }
                                    }

                                }

                            }

                        }
                    }

                    dialog.dismiss()

                }.setNegativeButton (R.string.cancel) {dialog, which -> dialog.dismiss()}

        return builder
    }


}

