/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import java.lang.ref.WeakReference

class EnableLocation(activityWeak: WeakReference<Activity>){

    private val TAG: String = this.javaClass.simpleName
    private var activity: Activity = activityWeak.get()!!
    private var permission_requestcode = 1
    private var permissions = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION)

    fun locationPermission() {
        if (ContextCompat.checkSelfPermission(activity.applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            grantAllPermissions()

        }
    }

    fun grantAllPermissions(){
        LogD(TAG, "grantAllPermissions")
        ActivityCompat.requestPermissions(activity, permissions, permission_requestcode)

    }

//    fun grantEachPermission(permission: String){
//        LogD(TAG, "grantEachPermission " + permission)
//        ActivityCompat.requestPermissions(activity, arrayOf(permission), permission_requestcode)
//    }

    private val lm = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    fun isLocationEnabled(): Boolean {

        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    fun enableLocation(){

        activity.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
    }


}