/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT

class SettingParamDialog: DialogFragment(){

    private val TAG = this.javaClass.simpleName

    /**
     * listener for cancel
     */
    private var mCancelListener: OnDialogCancelListener? = null

    /**
     * show dialog
     */
    private var mOnCallDialog: OnCallDialog? = null

    interface OnDialogCancelListener {
        fun onCancel()
    }

    interface OnCallDialog {
        fun getDialog(context: Context?): Dialog
    }

    fun newInstance(callDialog: OnCallDialog, cancelable: Boolean): SettingParamDialog {
        return newInstance(callDialog, cancelable, null)
    }

    fun newInstance(callDialog: OnCallDialog, cancelable: Boolean, cancelListener: OnDialogCancelListener?): SettingParamDialog {
        val instance = SettingParamDialog()
        instance.isCancelable = cancelable
        instance.mCancelListener = cancelListener
        instance.mOnCallDialog = callDialog
        return instance
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        when(mOnCallDialog == null){
            true -> {
                super.onCreate(savedInstanceState)
            }
        }
        return mOnCallDialog!!.getDialog(activity)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        when(dialog != null){
            true -> {
                val width = Resources.getSystem().displayMetrics.widthPixels
                val height = Resources.getSystem().displayMetrics.heightPixels
                val window = getDialog().window
                window.setLayout(width * 8 / 10, WRAP_CONTENT)
                val windowParams = window!!.attributes
                windowParams.dimAmount = 0.0f
                window.attributes = windowParams
            }
        }
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        when (mCancelListener != null) {
            true -> { mCancelListener!!.onCancel() }
        }
    }

}