/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Activity

import SSIDList
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import info
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import tracmo.tw.com.stationmanager.Dialog.DialogHelper
import tracmo.tw.com.stationmanager.Dialog.SettingParamDialog
import tracmo.tw.com.stationmanager.Entity.Logger
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Event.CallbackMessageEvent
import tracmo.tw.com.stationmanager.Event.SSIDCallbackEvent
import tracmo.tw.com.stationmanager.Event.SetWifiEvent
import tracmo.tw.com.stationmanager.Jobs
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DHCP
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_SSID
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_STATIC_IP
import tracmo.tw.com.stationmanager.Jobs.Companion.INDEX
import tracmo.tw.com.stationmanager.ListAdapter
import tracmo.tw.com.stationmanager.ListAdapter.Companion.SSID_LIST
import tracmo.tw.com.stationmanager.Listener.IDialogResultListener
import tracmo.tw.com.stationmanager.R

internal class WifiListActivity : AppCompatActivity() {

    private val TAG: String = this.javaClass.simpleName

    private lateinit var recyclerView: RecyclerView
    internal var pullToRefresh: SwipeRefreshLayout? = null

    private val dialogListener = object: IDialogResultListener<HashMap<String, String>> {
        override fun onDataResult(result: HashMap<String, String>?) {
            Logger.LogD(TAG, "dialogListener")
            when(result == null){
                true -> {
                    DialogHelper().showConfirmDialog(supportFragmentManager, getString(R.string.params_insufficient),
                            object: IDialogResultListener<Int> {
                                override fun onDataResult(result: Int?) {
                                    //do nothing
                                }
                            }, true,
                            object: SettingParamDialog.OnDialogCancelListener{
                                override fun onCancel() {
                                    //do nothing
                                }
                            })
                }
                false -> {
                    when(result!!.size){
                        2 -> {
                            //dhcp
                            Jobs(applicationContext).scheduleJobs(COMMAND_DHCP, result)

                        }
                        7 -> {
                            //static ip
                            Jobs(applicationContext).scheduleJobs(COMMAND_STATIC_IP, result)
                        }
                    }

                    //static ip
                }
            }


        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SSIDCallbackEvent){

//        Logger.LogD(TAG, "${SSIDList[event.index]}")
        recyclerView.adapter = ListAdapter(SSIDList, SSID_LIST)
        recyclerView.adapter.notifyDataSetChanged()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CallbackMessageEvent){
        when(event.msg){
            getString(R.string.disconnect) -> {
                startActivity(Intent(this, MainActivity::class.java))
            }
            getString(R.string.start_connect) -> {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }

    }

    /**
     * SetWifiEvent must pass index and name
     * */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SetWifiEvent){
        Logger.LogD(TAG, "onMessageEvent SetWifiEvent")
        DialogHelper().showWifiParamsDialog(supportFragmentManager, event, dialogListener, true)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifi_list)

        //refresh
        pullToRefresh = findViewById(R.id.pullToRefresh)
        pullToRefresh!!.setDistanceToTriggerSync(300)
        pullToRefresh!!.isRefreshing = false
        pullToRefresh!!.setOnRefreshListener {
            Logger.LogD(TAG, "onRefresh")
            SSIDList.clear()
            //SSID list command
            Jobs(applicationContext).scheduleJobs(COMMAND_SSID, null)

            LogD(TAG, "${pullToRefresh!!.isRefreshing} isRefreshing")
            val refreshTime = 3000L
            Handler().postDelayed(Runnable {
                pullToRefresh!!.isRefreshing = false
            }, refreshTime)
        }

        recyclerView = findViewById(R.id.list)
        recyclerView.layoutManager = LinearLayoutManager(this)
//        SSIDList[2] = "Test"
//        recyclerView.adapter = ListAdapter(SSIDList)

    }

    override fun onResume() {
        super.onResume()
        when (!EventBus.getDefault().isRegistered(this)) {
            true -> EventBus.getDefault().register(this@WifiListActivity)
        }
        recyclerView.adapter = ListAdapter(SSIDList, SSID_LIST)
        recyclerView.adapter.notifyDataSetChanged()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this@WifiListActivity)
        super.onStop()
    }

    fun onClick(v: View){

        //fab
        DialogHelper().showWifiParamsDialog(supportFragmentManager, null, dialogListener, true)

    }

}