/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import tracmo.tw.com.stationmanager.Listener.IPermission

class Receiver(permissionListener: IPermission): BroadcastReceiver(){

    private val listener = permissionListener
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent!!.action

        if(action == BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED ||
                action == LocationManager.MODE_CHANGED_ACTION){

            //refreshView() when current activity is PermissionActivity
            listener.result(-1, false)

        }
    }

}