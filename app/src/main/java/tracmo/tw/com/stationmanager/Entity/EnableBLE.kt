/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import java.lang.ref.WeakReference


/**
 * Created by J.Y on 2018/3/27.
 */

class EnableBLE(private val activityWeak: WeakReference<Activity>){

    private val TAG: String = this.javaClass.simpleName
    var bluetoothManager: BluetoothManager
    var bluetoothAdapter: BluetoothAdapter? = null
    private var activity: Activity = activityWeak.get()!!

    init {

        bluetoothManager = activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

    }

    fun isBLEEnable(): Boolean{
        if(bluetoothAdapter == null)
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter!!.isEnabled
    }


    fun enableBLE() {

        //check if device support ble
        if (!activity.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {

//            toast = Toast(activity, "Device has no BLE.")
            activity.finish()

        }

        if (Build.VERSION.SDK_INT >= 23) {
            LogD(TAG, "Build.VERSION.SDK_INT >= 23")
            EnableLocation(WeakReference(activity)).locationPermission()
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (bluetoothAdapter == null) {

//            isSupportBLE = false
            Toast.makeText(activity, "Device not support.", LENGTH_SHORT).show()

        } else{

            if(!bluetoothAdapter!!.isEnabled)
                bluetoothAdapter!!.enable()


        }

    }

}
