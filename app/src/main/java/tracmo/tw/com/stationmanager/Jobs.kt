/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.PersistableBundle
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD

class Jobs(con: Context){

    private val TAG = this.javaClass.simpleName
    private val context = con

    companion object {

        const val CONNECT = 1
        const val SCAN = 2
        //        const val STOP_SCAN = 3
        const val COMMAND_SSID = 4
        const val COMMAND_DHCP = 5
        const val COMMAND_STATIC_IP = 6
        const val COMMAND_OTA_STATUS = 11
        const val COMMAND_WIFI_STATUS = 12
        const val COMMAND_INTERNET_STATUS = 13
        const val COMMAND_RESTART_DEVICE = 14
        const val COMMAND_DISCONNECT_AND_STOP_ADVERTISING = 15
        const val COMMAND_LED_OFF = 16
        const val COMMAND_LED_ON = 17
        const val COMMAND_LED_FLASH_ON = 18
        const val COMMAND_LED_BLINK_ONCE = 19
        const val COMMAND_NETWORK_MODE = 20
        const val COMMAND_DNS_CONFIG = 21
        const val COMMAND_SSID_CONFIG = 22
        const val COMMAND_MANUFACTURER_INFORMATION = 23
        const val COMMAND_DEVICE_ADDRESS = 24
        const val COMMAND_MODEL_INDORMATION = 25
        const val COMMAND_HARDWARE_VERSION = 26
        const val COMMAND_FIRMWARE_VERSION = 27
        const val COMMAND_FCC_INFORMATION = 28
        const val COMMAND_IC_INFORMATION = 29
        const val COMMAND_NCC_INFORMATION = 30
        const val COMMAND_JRF_INFORMATION = 31

        const val NAME = "NAME"
        const val INDEX = "INDEX"
        const val PASSWORD = "PASSWORD"
        const val IP_ADDRESS = "IP_ADDRESS"
        const val SUBNET_MASK = "SUBNET_MASK"
        const val ROUTER = "ROUTER"
        const val DNS_1 = "DNS_1"
        const val DNS_2 = "DNS_2"
    }

    /**
     * real-time job
     *
     * Job id:
     * @param CONNECT = 1, connect to station via BLE
     * @param SCAN = 2, scan status of OTA, wifi, internet
     * @param STOP_SCAN = 3, stop periodically job
     * @param COMMAND_SSID = 4, request SSID list
     * @param COMMAND_DHCP = 5, set visible wifi via DHCP
     * @param COMMAND_STATIC_IP = 6, set visible wifi via static IP
     * Number 11 to 31 for cmd constants {@see CmdActivity#cmdMap}
     *
     * */
    fun scheduleJobs(num: Int, params: HashMap<String, String>?){
        val serviceComponent = ComponentName(context, T4JobService::class.java)
        val jobScheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val builder = JobInfo.Builder(num, serviceComponent)
        builder.setMinimumLatency(0)

        LogD(TAG, "$num num")
        val bundle = PersistableBundle()
        when(num){
            COMMAND_DHCP -> {
                when(params!!.isEmpty()){
                    false -> {
                        when(params[INDEX] == null){
                            true -> {
                                bundle.putString(NAME, params[NAME])
                                bundle.putString(PASSWORD, params[PASSWORD])
                            }
                            false -> {
                                bundle.putString(INDEX, params[INDEX])
                                bundle.putString(PASSWORD, params[PASSWORD])
                                LogD(TAG, "$num num ${params[INDEX]}")
                            }
                        }
                    }
                }
            }
            COMMAND_STATIC_IP -> {
                when(params!!.isEmpty()){
                    false -> {

                        when(params[INDEX] == null){
                            true -> {
                                bundle.putString(NAME, params[NAME])
                                bundle.putString(PASSWORD, params[PASSWORD])
                                bundle.putString(IP_ADDRESS, params[IP_ADDRESS])
                                bundle.putString(SUBNET_MASK, params[SUBNET_MASK])
                                bundle.putString(ROUTER, params[ROUTER])
                                bundle.putString(DNS_1, params[DNS_1])
                                bundle.putString(DNS_2, params[DNS_2])
                            }
                            false -> {
                                bundle.putString(INDEX, params[INDEX])
                                bundle.putString(PASSWORD, params[PASSWORD])
                                bundle.putString(IP_ADDRESS, params[IP_ADDRESS])
                                bundle.putString(SUBNET_MASK, params[SUBNET_MASK])
                                bundle.putString(ROUTER, params[ROUTER])
                                bundle.putString(DNS_1, params[DNS_1])
                                bundle.putString(DNS_2, params[DNS_2])
                            }
                        }
                    }
                }

            }

        }
        jobScheduler.schedule(builder.setExtras(bundle).build())
    }

}

