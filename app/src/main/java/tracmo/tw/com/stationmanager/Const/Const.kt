/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */
@file:JvmName("Constants")
@file:Suppress("PropertyName")

import android.util.SparseArray
import tracmo.tw.com.stationmanager.Entity.T4Info
import java.util.*

@JvmField val CALLBACK_EVENT = "EVENT"
@JvmField val CALLBACK_ADDRESS = "ADDRESS"
@JvmField val CALLBACK_DATA = "DATA"

//unique object
@JvmField val info = T4Info()
@JvmField val SSIDList = HashMap<Int, String>()

// RX
@JvmField val ACTION_TRACMO_CONNECTED = "connected"
@JvmField val ACTION_TRACMO_DISCONNECTED = "disconnected"
@JvmField val ACTION_TRACMO_READ = "read"
@JvmField val ACTION_TRACMO_DISCOVERY_OK = "discovery"
@JvmField val ACTION_TRACMO_CALLBACK = "callback"
@JvmField val ACTION_TRACMO_BUTTON = "button"

//uuid
@JvmField val UUID_PRIMARY_SERVICE = UUID.fromString("E8008901-4143-5453-5162-6C696E6B73EC")
@JvmField val UUID_COMMAND_CHARACTERISTIC = UUID.fromString("E8009B01-4143-5453-5162-6C696E6B73EC") //write
@JvmField val UUID_CALLBACK_CHARACTERISTIC = UUID.fromString("E8009B02-4143-5453-5162-6C696E6B73EC") //read, notify
@JvmField val UUID_CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

@JvmField val ADVERTISING_TRACMO_HEADER = byteArrayOf(0x02)
@JvmField val ADVERTISING_TRACMO_NAME = byteArrayOf(0x09)
@JvmField val ADVERTISING_TRACMO_COMPANY_ID = byteArrayOf(0xF7.toByte(), 0x05, 0x54, 0x34)

//@JvmField val UUID_TRACMO = byteArrayOf(0xEC.toByte(), 0x73, 0x6B, 0x6E, 0x69, 0x6C, 0x62, 0x51, 0x53, 0x54, 0x43, 0x41, 0xFF.toByte(), 0xFF.toByte(), 0x00, 0xE8.toByte())

//cmd
@JvmField val COMMAND_REQUEST_MANUFACTURER_INFO = byteArrayOf(0x06, 0x03)
@JvmField val COMMAND_REQUEST_DEVICE_ADDRESS = byteArrayOf(0x06, 0x04)
@JvmField val COMMAND_REQUEST_MODEL_INFO = byteArrayOf(0x06, 0x05)
@JvmField val COMMAND_REQUEST_HARDWARE_VERSION = byteArrayOf(0x06, 0x06)
@JvmField val COMMAND_REQUEST_FIRMWARE_VERSION = byteArrayOf(0x06, 0x07)
@JvmField val COMMAND_REQUEST_FCC_INFO = byteArrayOf(0x06, 0x08)
@JvmField val COMMAND_REQUEST_IC_INFO = byteArrayOf(0x06, 0x09)
@JvmField val COMMAND_REQUEST_NCC_INFO = byteArrayOf(0x06, 0x0A)
@JvmField val COMMAND_REQUEST_JRF_INFO = byteArrayOf(0x06, 0x0D)

@JvmField val COMMAND_REQUEST_RESTART_DEVICE = byteArrayOf(0x2F, 0x03, 0x00)
@JvmField val COMMAND_REQUEST_DISCONNECT_AND_STOP_ADVERTISING = byteArrayOf(0x2F, 0x06, 0x00)
@JvmField val COMMAND_REQUEST_LED_OFF = byteArrayOf(0x2F, 0x07, 0x00)
@JvmField val COMMAND_REQUEST_LED_ON = byteArrayOf(0x2F, 0x07, 0x01)
@JvmField val COMMAND_REQUEST_LED_FLASH_ON = byteArrayOf(0x2F, 0x07, 0x02)
@JvmField val COMMAND_REQUEST_LED_BLINK_ONCE = byteArrayOf(0x2F, 0x07, 0x03)

@JvmField val COMMAND_REQUEST_OTA_STATUS = byteArrayOf(0x2F, 0x08)
@JvmField val COMMAND_REQUEST_WIFI_STATUS = byteArrayOf(0x2F, 0x09)
@JvmField val COMMAND_REQUEST_INTERNET_STATUS = byteArrayOf(0x2F, 0x0B)

//cmd header = callback header
@JvmField val COMMAND_REQUEST_NETWORK_MODE = byteArrayOf(0x70, 0x01)
@JvmField val COMMAND_REQUEST_DNS_CONFIG = byteArrayOf(0x70, 0x02)
@JvmField val COMMAND_REQUEST_SSID_CONFIG = byteArrayOf(0x70, 0x03)
@JvmField val COMMAND_REQUEST_SSID_LIST = byteArrayOf(0x70, 0x04)

//cmd
@JvmField val COMMAND_REQUEST_CONFIGURE_SSID_NAME = byteArrayOf(0x70, 0x05)
@JvmField val COMMAND_REQUEST_CONFIGURE_SSID_INDEX = byteArrayOf(0x70, 0x06)
@JvmField val COMMAND_REQUEST_CONFIGURE_WIFI_PASSWORD = byteArrayOf(0x70, 0x07)
@JvmField val COMMAND_REQUEST_CONFIGURE_NETWORK = byteArrayOf(0x70, 0x08)
@JvmField val COMMAND_REQUEST_CONFIGURE_DNS = byteArrayOf(0x70, 0x09)
@JvmField val COMMAND_REQUEST_CONFIGURE_HTTP_DEBUG_MODE = byteArrayOf(0x70, 0x0A)

//event notification
@JvmField val CALLBACK_NOTIFICATION_BUTTON_PUSHED = byteArrayOf(0x07, 0x01)
@JvmField val CALLBACK_NOTIFICATION_BUTTON_RELEASED = byteArrayOf(0x07, 0x02)
@JvmField val CALLBACK_NOTIFICATION_WIFI_CONNECTED = byteArrayOf(0x07, 0x03)
@JvmField val CALLBACK_NOTIFICATION_WIFI_DISCONNECTED = byteArrayOf(0x07, 0x04)
@JvmField val CALLBACK_NOTIFICATION_OTA_WAITED = byteArrayOf(0x07, 0x05)
@JvmField val CALLBACK_NOTIFICATION_OTA_PROCESS = byteArrayOf(0x07, 0x06)
@JvmField val CALLBACK_NOTIFICATION_OTA_COMPLETED = byteArrayOf(0x07, 0x07)
@JvmField val CALLBACK_NOTIFICATION_OTA_FAILED = byteArrayOf(0x07, 0x08)
@JvmField val CALLBACK_NOTIFICATION_INTERNET_OK = byteArrayOf(0x07, 0x09)
@JvmField val CALLBACK_NOTIFICATION_INTERNET_ERROR = byteArrayOf(0x07, 0x0A)

//header + ASCII
@JvmField val CALLBACK_MANUFACTURER_INFO = byteArrayOf(0x06, 0x03)
@JvmField val CALLBACK_DEVICE_ADDRESS = byteArrayOf(0x06, 0x04)
@JvmField val CALLBACK_MODEL_INFO = byteArrayOf(0x06, 0x05)
@JvmField val CALLBACK_HARDWARE_VERSION = byteArrayOf(0x06, 0x06)
@JvmField val CALLBACK_FIRMWARE_VERSION = byteArrayOf(0x06, 0x07)
@JvmField val CALLBACK_FCC_INFO = byteArrayOf(0x06, 0x08)
@JvmField val CALLBACK_IC_INFO = byteArrayOf(0x06, 0x09)
@JvmField val CALLBACK_NCC_INFO = byteArrayOf(0x06, 0x0A)
@JvmField val CALLBACK_JRF_INFO = byteArrayOf(0x06, 0x0D)
