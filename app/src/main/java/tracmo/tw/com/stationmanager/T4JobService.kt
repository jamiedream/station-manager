/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager

import ACTION_TRACMO_CALLBACK
import ACTION_TRACMO_CONNECTED
import ACTION_TRACMO_DISCONNECTED
import ACTION_TRACMO_DISCOVERY_OK
import CALLBACK_ADDRESS
import CALLBACK_DATA
import CALLBACK_DEVICE_ADDRESS
import CALLBACK_EVENT
import CALLBACK_FCC_INFO
import CALLBACK_FIRMWARE_VERSION
import CALLBACK_HARDWARE_VERSION
import CALLBACK_IC_INFO
import CALLBACK_JRF_INFO
import CALLBACK_MANUFACTURER_INFO
import CALLBACK_MODEL_INFO
import CALLBACK_NCC_INFO
import CALLBACK_NOTIFICATION_BUTTON_PUSHED
import CALLBACK_NOTIFICATION_BUTTON_RELEASED
import CALLBACK_NOTIFICATION_INTERNET_ERROR
import CALLBACK_NOTIFICATION_INTERNET_OK
import CALLBACK_NOTIFICATION_OTA_COMPLETED
import CALLBACK_NOTIFICATION_OTA_FAILED
import CALLBACK_NOTIFICATION_OTA_PROCESS
import CALLBACK_NOTIFICATION_OTA_WAITED
import CALLBACK_NOTIFICATION_WIFI_CONNECTED
import CALLBACK_NOTIFICATION_WIFI_DISCONNECTED
import COMMAND_REQUEST_CONFIGURE_DNS
import COMMAND_REQUEST_CONFIGURE_NETWORK
import COMMAND_REQUEST_CONFIGURE_SSID_INDEX
import COMMAND_REQUEST_CONFIGURE_WIFI_PASSWORD
import COMMAND_REQUEST_DEVICE_ADDRESS
import COMMAND_REQUEST_DISCONNECT_AND_STOP_ADVERTISING
import COMMAND_REQUEST_DNS_CONFIG
import COMMAND_REQUEST_FCC_INFO
import COMMAND_REQUEST_FIRMWARE_VERSION
import COMMAND_REQUEST_HARDWARE_VERSION
import COMMAND_REQUEST_IC_INFO
import COMMAND_REQUEST_INTERNET_STATUS
import COMMAND_REQUEST_JRF_INFO
import COMMAND_REQUEST_LED_BLINK_ONCE
import COMMAND_REQUEST_LED_FLASH_ON
import COMMAND_REQUEST_LED_OFF
import COMMAND_REQUEST_LED_ON
import COMMAND_REQUEST_MANUFACTURER_INFO
import COMMAND_REQUEST_MODEL_INFO
import COMMAND_REQUEST_NCC_INFO
import COMMAND_REQUEST_NETWORK_MODE
import COMMAND_REQUEST_OTA_STATUS
import COMMAND_REQUEST_RESTART_DEVICE
import COMMAND_REQUEST_SSID_CONFIG
import COMMAND_REQUEST_SSID_LIST
import COMMAND_REQUEST_WIFI_STATUS
import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Handler
import android.os.Looper
import com.google.android.gms.common.util.Hex
import info
import org.greenrobot.eventbus.EventBus
import tracmo.tw.com.stationmanager.Callback.GattCallback
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Event.CallbackMessageEvent
import tracmo.tw.com.stationmanager.Event.SSIDCallbackEvent
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DEVICE_ADDRESS
import tracmo.tw.com.stationmanager.Listener.IGattCallbackListener
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DHCP
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DISCONNECT_AND_STOP_ADVERTISING
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DNS_CONFIG
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_FCC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_FIRMWARE_VERSION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_HARDWARE_VERSION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_IC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_INTERNET_STATUS
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_JRF_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_BLINK_ONCE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_FLASH_ON
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_OFF
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_ON
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_MANUFACTURER_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_MODEL_INDORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_NCC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_NETWORK_MODE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_OTA_STATUS
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_RESTART_DEVICE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_SSID
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_SSID_CONFIG
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_STATIC_IP
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_WIFI_STATUS
import tracmo.tw.com.stationmanager.Jobs.Companion.CONNECT
import tracmo.tw.com.stationmanager.Jobs.Companion.DNS_1
import tracmo.tw.com.stationmanager.Jobs.Companion.DNS_2
import tracmo.tw.com.stationmanager.Jobs.Companion.INDEX
import tracmo.tw.com.stationmanager.Jobs.Companion.IP_ADDRESS
import tracmo.tw.com.stationmanager.Jobs.Companion.NAME
import tracmo.tw.com.stationmanager.Jobs.Companion.PASSWORD
import tracmo.tw.com.stationmanager.Jobs.Companion.ROUTER
import tracmo.tw.com.stationmanager.Jobs.Companion.SCAN
import tracmo.tw.com.stationmanager.Jobs.Companion.SUBNET_MASK
import java.lang.StringBuilder
import java.nio.charset.StandardCharsets
import java.util.*


class T4JobService: JobService() {

    private val TAG: String = this.javaClass.simpleName

    companion object {
        var isConnect = false
    }

    override fun onStartJob(params: JobParameters): Boolean {

        val jobId = params.jobId
        Handler().post {

            when (jobId) {

                CONNECT -> {
                    isConnect = connect()
                }
                SCAN -> {
                    sendCommand(COMMAND_REQUEST_OTA_STATUS)
                    sendCommand(COMMAND_REQUEST_WIFI_STATUS)
                    sendCommand(COMMAND_REQUEST_INTERNET_STATUS)
                }

                COMMAND_SSID -> {
                    sendCommand(COMMAND_REQUEST_SSID_LIST)
                }
                COMMAND_DHCP -> {

//                    LogD(TAG, params.extras.getString(INDEX))
//                    LogD(TAG, params.extras.getString(NAME))
//                    LogD(TAG, params.extras.getString(PASSWORD))

                    when(params.extras.getString(INDEX) == null){
                        false -> {
                            //exist, index
                            val index = ByteArray(1)
                            index[0] = params.extras.getString(INDEX).toByte()
                            LogD(TAG, "${index[0]}")
                            val indexArray = addAll(COMMAND_REQUEST_CONFIGURE_SSID_INDEX, index)
                            sendCommand(indexArray)
                        }
                        true -> {
                            //not exist, name
                            val name = params.extras.getString(NAME).toByteArray(StandardCharsets.US_ASCII)
                            name.forEach { LogD(TAG, "$it name") }
                            val nameArray = addAll(COMMAND_REQUEST_CONFIGURE_SSID_INDEX, name)
                            sendCommand(nameArray)
                        }
                    }

                    val password = params.extras.getString(PASSWORD).toByteArray(StandardCharsets.US_ASCII)

                    val passwordArray = addAll(COMMAND_REQUEST_CONFIGURE_WIFI_PASSWORD, password)
                    val networkArray = addAll(COMMAND_REQUEST_CONFIGURE_NETWORK, byteArrayOf(0x00))

                    sendCommand(passwordArray)
                    sendCommand(networkArray)
                }

                COMMAND_STATIC_IP -> {

//                    LogD(TAG, params.extras.getString(INDEX))
//                    LogD(TAG, params.extras.getString(NAME))
//                    LogD(TAG, params.extras.getString(PASSWORD))
//                    LogD(TAG, params.extras.getString(IP_ADDRESS))
//                    LogD(TAG, params.extras.getString(SUBNET_MASK))
//                    LogD(TAG, params.extras.getString(ROUTER))
//                    LogD(TAG, params.extras.getString(DNS_1))
//                    LogD(TAG, params.extras.getString(DNS_2))

                    when(params.extras.getString(INDEX) == null){
                        false -> {
                            //exist, index
                            val index = ByteArray(1)
                            index[0] = params.extras.getString(INDEX).toByte()
                            LogD(TAG, "${index[0]}")
                            val indexArray = addAll(COMMAND_REQUEST_CONFIGURE_SSID_INDEX, index)
                            sendCommand(indexArray)
                        }
                        true -> {
                            //not exist, name
                            val name = params.extras.getString(NAME).toByteArray(StandardCharsets.US_ASCII)
                            name.forEach { LogD(TAG, "$it name") }
                            val nameArray = addAll(COMMAND_REQUEST_CONFIGURE_SSID_INDEX, name)
                            sendCommand(nameArray)
                        }
                    }

                    val password = params.extras.getString(PASSWORD).toByteArray(StandardCharsets.US_ASCII)
                    val ipAddress = getStaticByteArray(params.extras.getString(IP_ADDRESS))
                    val subnetMask = getStaticByteArray(params.extras.getString(SUBNET_MASK))
                    val router = getStaticByteArray(params.extras.getString(ROUTER))
                    val dns1 = getStaticByteArray(params.extras.getString(DNS_1))
                    val dns2 = getStaticByteArray(params.extras.getString(DNS_2))

                    val passwordArray = addAll(COMMAND_REQUEST_CONFIGURE_WIFI_PASSWORD, password)

                    val networkHeader = addAll(COMMAND_REQUEST_CONFIGURE_NETWORK, byteArrayOf(0x01))
                    val networkStaticIP = addAll(networkHeader, ipAddress)
                    val networkSubnetMask = addAll(networkStaticIP, subnetMask)
                    val networkArray = addAll(networkSubnetMask, router)
//                    networkArray.forEach {
//                        LogD(TAG, "$it networkArray")
//                    }

                    val dns1Array = addAll(COMMAND_REQUEST_CONFIGURE_DNS, dns1)
                    val dnsArray = addAll(dns1Array, dns2)
//                    dnsArray.forEach {
//                        LogD(TAG, "$it dnsArray")
//                    }

                    sendCommand(passwordArray)
                    sendCommand(networkArray)
                    sendCommand(dnsArray)
                }

                COMMAND_OTA_STATUS -> sendCommand(COMMAND_REQUEST_OTA_STATUS)
                COMMAND_WIFI_STATUS -> sendCommand(COMMAND_REQUEST_WIFI_STATUS)
                COMMAND_INTERNET_STATUS -> sendCommand(COMMAND_REQUEST_INTERNET_STATUS)
                COMMAND_RESTART_DEVICE -> sendCommand(COMMAND_REQUEST_RESTART_DEVICE)
                COMMAND_DISCONNECT_AND_STOP_ADVERTISING -> sendCommand(COMMAND_REQUEST_DISCONNECT_AND_STOP_ADVERTISING)
                COMMAND_LED_OFF -> sendCommand(COMMAND_REQUEST_LED_OFF)
                COMMAND_LED_ON -> sendCommand(COMMAND_REQUEST_LED_ON)
                COMMAND_LED_FLASH_ON -> sendCommand(COMMAND_REQUEST_LED_FLASH_ON)
                COMMAND_LED_BLINK_ONCE -> sendCommand(COMMAND_REQUEST_LED_BLINK_ONCE)
                COMMAND_NETWORK_MODE -> sendCommand(COMMAND_REQUEST_NETWORK_MODE)
                COMMAND_DNS_CONFIG -> sendCommand(COMMAND_REQUEST_DNS_CONFIG)
                COMMAND_SSID_CONFIG -> sendCommand(COMMAND_REQUEST_SSID_CONFIG)
                COMMAND_MANUFACTURER_INFORMATION -> sendCommand(COMMAND_REQUEST_MANUFACTURER_INFO)
                COMMAND_DEVICE_ADDRESS -> sendCommand(COMMAND_REQUEST_DEVICE_ADDRESS)
                COMMAND_MODEL_INDORMATION -> sendCommand(COMMAND_REQUEST_MODEL_INFO)
                COMMAND_HARDWARE_VERSION -> sendCommand(COMMAND_REQUEST_HARDWARE_VERSION)
                COMMAND_FIRMWARE_VERSION -> sendCommand(COMMAND_REQUEST_FIRMWARE_VERSION)
                COMMAND_FCC_INFORMATION -> sendCommand(COMMAND_REQUEST_FCC_INFO)
                COMMAND_IC_INFORMATION -> sendCommand(COMMAND_REQUEST_IC_INFO)
                COMMAND_NCC_INFORMATION -> sendCommand(COMMAND_REQUEST_NCC_INFO)
                COMMAND_JRF_INFORMATION -> sendCommand(COMMAND_REQUEST_JRF_INFO)

            }

            jobFinished(params, false)
        }

        LogD(TAG, "on start job: ${params.jobId}")
        return false
    }


    override fun onStopJob(params: JobParameters?): Boolean {

        return false

    }

    private fun addAll(array1: ByteArray, array2: ByteArray): ByteArray {
        val joinedArray = Arrays.copyOf(array1, array1.size + array2.size)
        System.arraycopy(array2, 0, joinedArray, array1.size, array2.size)
        return joinedArray
    }

    private fun getStaticByteArray(ip: String): ByteArray {

        //xx.xx.xx.xx
        val stringArray = ip.split(".")
        val byteArray: ByteArray

        when (stringArray.size == 4) {
            true -> {
                byteArray = ByteArray(4)
                var num = 0
                stringArray.forEach {
                    //                    LogD(TAG, "getStaticByteArray $it") //255
                    val t0 = it.toInt().toByte()
                    byteArray[num] = t0
                    num++
                }
            }
        //x, let array empty
            false -> {
                LogD(TAG, "getStaticByteArray string length error")
                byteArray = ByteArray(1)
            }
        }
//        byteArray.forEach { LogD(TAG, "getStaticByteArray $it") }
        return byteArray
    }

    fun connect(): Boolean {

        var isConnect = false
        when (info.gattCallback != null) {
            true -> {
                isConnect = info.connect(info.gattCallback, this)
            }
            false -> {
                val listener = object : IGattCallbackListener {
                    override fun callbackResult(dataMap: HashMap<String, Any>) {
                        LogD(TAG, "address=" + dataMap[CALLBACK_ADDRESS])
                        val handler = Handler(Looper.getMainLooper()) // need: Looper.getMainLooper()
                        handler.post { callbackHandler(dataMap) }
                    }
                }

                val mGattCallback = GattCallback(listener)
                isConnect = info.connect(mGattCallback, this)

            }
        }

        return isConnect
    }

    fun callbackHandler(dataMap: HashMap<String, Any>) {
        when (dataMap.isEmpty()) { true -> return
        }
        val addr = dataMap[CALLBACK_ADDRESS] as String
        val event = dataMap[CALLBACK_EVENT] as String
//        LogD(TAG, "$addr is null or blank $event")
        when (addr.isBlank() || event.isBlank()) { true -> return
        }
//        LogD(TAG, "$addr addr")
        LogD(TAG, "$event event")

        when (event) {
            ACTION_TRACMO_DISCOVERY_OK -> {
                Thread.sleep(1000)
                when (info.gattCallback != null) {
                    true -> {
                        sendCommand(COMMAND_REQUEST_OTA_STATUS)
                        sendCommand(COMMAND_REQUEST_WIFI_STATUS)
                        sendCommand(COMMAND_REQUEST_INTERNET_STATUS)
                        sendCommand(COMMAND_REQUEST_SSID_LIST)
                    }
                }
            }
            ACTION_TRACMO_CONNECTED -> {
                Jobs(applicationContext).scheduleJobs(SCAN, null)
                EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.connect)))
            }
            ACTION_TRACMO_DISCONNECTED -> {
                isConnect = false
                info.isT4 = false
                EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.disconnect)))
            }
            ACTION_TRACMO_CALLBACK -> {
                val data = dataMap[CALLBACK_DATA] as ByteArray
                LogD(TAG, "$data data")
                when (data.size >= 2) {
                    true -> {
                        when {
                            //custom status callback
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_BUTTON_PUSHED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.button_push)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_BUTTON_RELEASED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.button_released)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_WIFI_CONNECTED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.wifi_connected)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_WIFI_DISCONNECTED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.wifi_disconnected)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_OTA_WAITED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ota_waiting)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_OTA_PROCESS) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ota_proceed)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_OTA_COMPLETED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ota_completed)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_OTA_FAILED) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ota_failed)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_INTERNET_OK) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.internet_ok)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NOTIFICATION_INTERNET_ERROR) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.internet_error)))
                            //hex string
                            Arrays.equals(Arrays.copyOf(data, 2), COMMAND_REQUEST_NETWORK_MODE) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.network_mode) + " : " + byteArrayToHexString(data)))
                            Arrays.equals(Arrays.copyOf(data, 2), COMMAND_REQUEST_DNS_CONFIG) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.dns_config) + " : " + byteArrayToHexString(data)))
                            //ascii
                            Arrays.equals(Arrays.copyOf(data, 2), COMMAND_REQUEST_SSID_CONFIG) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ssid_config) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_MANUFACTURER_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.manufacturer_info) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_DEVICE_ADDRESS) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.device_address) + " : " + byteArrayToHexString(data)))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_MODEL_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.model_info) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_HARDWARE_VERSION) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.hardware_version) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_FIRMWARE_VERSION) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.firmware_version) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_FCC_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.fcc_info) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_IC_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ic_info) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_NCC_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ncc_info) + " : " + String(data.copyOfRange(2, data.size))))
                            Arrays.equals(Arrays.copyOf(data, 2), CALLBACK_JRF_INFO) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.jrf_info) + " : " +  String(data.copyOfRange(2, data.size))))
                            //ssid list
                            Arrays.equals(Arrays.copyOf(data, 2), COMMAND_REQUEST_SSID_LIST) -> {
                                //start wifi list activity
                                EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ssid_list_index)))

                                val newData = data.copyOfRange(2, data.size)
                                val index = newData[0].toInt()
                                val name = String(newData.copyOfRange(1, newData.size))
//                                LogD(TAG, "${data.size} data size")
//                                LogD(TAG, "${newData.size} new data size")
                                LogD(TAG, "$index index")
                                LogD(TAG, "$name name")
                                //wifi list
                                EventBus.getDefault().post(SSIDCallbackEvent(index, name))
                            }
                        }
                    }

                }
            }
        }

    }

    private fun sendCommand(value: ByteArray) {
        LogD(TAG, "${info.gattCallback} info.gattCallback")
        val commandCharacteristics = info.gattCallback!!.getCommandCharacteristic()
        LogD(TAG, "$commandCharacteristics commandCharacteristics")
        when(commandCharacteristics == null) { true -> return }
        commandCharacteristics!!.value = value
        when (info.gatt!!.writeCharacteristic(commandCharacteristics)) {
            true -> {
                LogD(TAG, "write success")
                when {
                    value.contentEquals(COMMAND_REQUEST_OTA_STATUS) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ota_status)))
                    value.contentEquals(COMMAND_REQUEST_WIFI_STATUS) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.wifi_status)))
                    value.contentEquals(COMMAND_REQUEST_INTERNET_STATUS) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.internet_status)))
                    value.contentEquals(COMMAND_REQUEST_SSID_LIST) -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.ssid_list)))
                    value[1] == COMMAND_REQUEST_CONFIGURE_SSID_INDEX[1] && value.size == 3 -> EventBus.getDefault().post(CallbackMessageEvent(getString(R.string.start_connect))) //index wrote, start scan status
                }
                Thread.sleep(200)
            }

            false -> LogD(TAG, "write failed")
        }
    }

    private fun byteArrayToHexString(array: ByteArray): StringBuilder{

        val newData = array.copyOfRange(2, array.size)
        val info = StringBuilder()
        newData.forEach { info.append(String.format("%02X ", it)) }
        LogD(TAG, "$info info")

        return info

    }

}