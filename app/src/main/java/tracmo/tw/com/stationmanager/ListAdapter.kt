/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.greenrobot.eventbus.EventBus
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogI
import tracmo.tw.com.stationmanager.Event.CMDEvent
import tracmo.tw.com.stationmanager.Event.SetWifiEvent
import tracmo.tw.com.stationmanager.Listener.IPermission


class ListAdapter: RecyclerView.Adapter<ListAdapter.ViewHolder>{

    private val TAG: String = this.javaClass.simpleName

    companion object {
        val CMD = "CMD"
        val SSID_LIST = "SSID_LIST"
        val PERMISSION = "PERMISSION"
        private lateinit var TYPE: String
    }

    private var list: HashMap<*, *>
    private lateinit var listener: IPermission

    constructor(map: HashMap<Int, String>, type: String){
        list = map
        TYPE = type
    }

    constructor(map: HashMap<String, Boolean>, permissionListener: IPermission, type: String){
        list = map
        TYPE = type
        listener = permissionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_list, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(TYPE){
            CMD, SSID_LIST -> {
                holder.item.id = list.keys.elementAt(position) as Int
                holder.item.text = list.values.elementAt(position) as String
                LogD(TAG, "${list.values.elementAt(position)}   ${holder.item.id}")
            }
            PERMISSION -> {
                LogI(TAG, list.keys.elementAt(position) as String)
                val isChecked = list.get(list.keys.elementAt(position))!!
                holder.item.text = list.keys.elementAt(position) as String
                holder.check.isSelected = isChecked as Boolean
                holder.check.setOnClickListener{
                    when(isChecked){
                        false -> listener.result(position, true)
                    }
                }
            }
        }


    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v) {

        private val TAG = this.javaClass.simpleName
        var item: TextView = v.findViewById(R.id.text)
        var check: ImageView = v.findViewById(R.id.status)

        init {
            when(TYPE){
                CMD -> item.setOnClickListener{
                    //todo
                    //intent to set DHCP or static IP
                    LogD(TAG, "${item.id} ${item.text} click")
                    EventBus.getDefault().post(CMDEvent(item.text.toString()))
                }
                SSID_LIST -> item.setOnClickListener{
                    //intent to set DHCP or static IP
                    LogD(TAG, "${item.id} ${item.text} click")
                    EventBus.getDefault().post(SetWifiEvent(item.id, item.text.toString()))
                }
                PERMISSION -> check.visibility = View.VISIBLE
            }
        }

    }

}