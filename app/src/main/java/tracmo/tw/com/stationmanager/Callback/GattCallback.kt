/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Callback

import ACTION_TRACMO_CALLBACK
import ACTION_TRACMO_CONNECTED
import ACTION_TRACMO_DISCONNECTED
import ACTION_TRACMO_DISCOVERY_OK
import ACTION_TRACMO_READ
import CALLBACK_ADDRESS
import CALLBACK_DATA
import CALLBACK_EVENT
import UUID_CALLBACK_CHARACTERISTIC
import UUID_CLIENT_CHARACTERISTIC_CONFIG
import UUID_COMMAND_CHARACTERISTIC
import UUID_PRIMARY_SERVICE
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import info
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogV
import tracmo.tw.com.stationmanager.Listener.IGattCallbackListener
import java.math.BigInteger
import java.util.*

class GattCallback(listener: IGattCallbackListener): BluetoothGattCallback(){

    private val TAG = this.javaClass.simpleName
    private val gattListener = listener
    private var commandCharacteristics: BluetoothGattCharacteristic? = null
    private var callbackCharacteristics: BluetoothGattCharacteristic? = null

    fun getCommandCharacteristic(): BluetoothGattCharacteristic? {
        return commandCharacteristics
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
        super.onConnectionStateChange(gatt, status, newState)
        LogD(TAG, "onConnectionStateChange")
        when(info.macAddress == gatt!!.device.address){
            true ->{
            LogV(TAG, "onConnectionStateChange ${gatt.device.address}")
            when(newState){
                BluetoothGatt.STATE_CONNECTED -> {
                    LogD(TAG, "Bluetooth Gatt Connected ${gatt.device.address}")
                    gattListener.callbackResult(gattGotCallback(gatt.device.address, ACTION_TRACMO_CONNECTED, 0))
                    gatt.discoverServices()
                }
                BluetoothGatt.STATE_CONNECTING -> LogD(TAG, "Bluetooth Gatt Connecting ${gatt.device.address}")
                BluetoothGatt.STATE_DISCONNECTED -> {
                    LogD(TAG, "Bluetooth Gatt Disconnected ${gatt.device.address}")
                    gattListener.callbackResult(gattGotCallback(gatt.device.address, ACTION_TRACMO_DISCONNECTED, 0))

                }
            }
            }
            false -> LogD(TAG, "${info.macAddress} != ${gatt.device.address}")
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
        super.onServicesDiscovered(gatt, status)
        LogD(TAG, "onServicesDiscovered")
        when(status){
            0 -> {
                LogV(TAG, "onServiceDiscovered - success: " + gatt!!.device.address)
                val primaryService = gatt.getService(UUID_PRIMARY_SERVICE)
                when{
                    primaryService != null ->{
                        callbackCharacteristics = primaryService.getCharacteristic(UUID_CALLBACK_CHARACTERISTIC)
                        commandCharacteristics = primaryService.getCharacteristic(UUID_COMMAND_CHARACTERISTIC)

                        when{
                            commandCharacteristics != null && callbackCharacteristics != null ->{
                                LogV(TAG, "set notify on:" + gatt.device.address)
                                when(setCharacteristicNotification(gatt, callbackCharacteristics)){
                                    true -> gattListener.callbackResult(gattGotCallback(gatt.device.address, ACTION_TRACMO_DISCOVERY_OK, 0))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setCharacteristicNotification(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?): Boolean {

        when(gatt == null || characteristic == null){
            true -> return false
        }

        when(gatt!!.setCharacteristicNotification(characteristic, true)) {
            true -> {
                val descriptor = characteristic!!.getDescriptor(UUID_CLIENT_CHARACTERISTIC_CONFIG)
                descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                return gatt.writeDescriptor(descriptor)
            }
        }

        return false
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
        super.onCharacteristicRead(gatt, characteristic, status)
        LogD(TAG, "onCharacteristicRead")
        when(status){
            BluetoothGatt.GATT_SUCCESS -> {
                LogD(TAG, "onCharacteristicRead - success: " + gatt!!.device.address)
                when(characteristic!! == callbackCharacteristics){
                    true -> {
                        gattListener.callbackResult(gattGotCallback(gatt.device.address, ACTION_TRACMO_READ,
                                characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)))
                    }
                }
            }
        }
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
        super.onCharacteristicChanged(gatt, characteristic)

        LogD(TAG, "onCharacteristicChanged: " + BigInteger(1, characteristic!!.value).toString(16))

        val addr = gatt!!.device.address
        val data = characteristic.value

        //handle callback cmd
        when(characteristic.uuid == UUID_CALLBACK_CHARACTERISTIC){
            true -> {
                LogD(TAG, "onCharacteristicChanged UUID_CALLBACK")
//                data.forEach {
//                    LogD(TAG, "onCharacteristicChanged " + String.format("%02X", it))
//                }
                gattListener.callbackResult(gattGotCallback(addr, ACTION_TRACMO_CALLBACK, data))
            }
        }

    }

    /**
     * data: value from characteristics. If data == 0, means no data.
     * **/
    private fun gattGotCallback(addr: String, event: String, data: Any): HashMap<String, Any>{

        val hashMap = HashMap<String, Any>()
        hashMap[CALLBACK_ADDRESS] = addr
        hashMap[CALLBACK_EVENT] = event
        hashMap[CALLBACK_DATA] = data

        return hashMap

    }





}