/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Dialog

import android.content.Context
import android.content.res.Resources
import android.util.AttributeSet
import android.widget.ScrollView
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD

class CustomMaxHeightScrollView : ScrollView {

    private val TAG = this.javaClass.simpleName
    private var maxHeight = 0

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    init {
        maxHeight = Resources.getSystem().displayMetrics.heightPixels * 5 / 10
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        var customHeightSpec = heightMeasureSpec
        when(MeasureSpec.getSize(heightMeasureSpec) > maxHeight){
            true -> customHeightSpec =  MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST)
        }

        super.onMeasure(widthMeasureSpec, customHeightSpec)

    }
}