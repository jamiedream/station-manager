/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Activity

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import info
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import tracmo.tw.com.stationmanager.Callback.ScanCallback
import tracmo.tw.com.stationmanager.Entity.EnableBLE
import tracmo.tw.com.stationmanager.Entity.EnableLocation
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Entity.T4Info
import tracmo.tw.com.stationmanager.Event.CallbackMessageEvent
import tracmo.tw.com.stationmanager.Jobs
import tracmo.tw.com.stationmanager.Jobs.Companion.CONNECT
import tracmo.tw.com.stationmanager.Jobs.Companion.SCAN
import tracmo.tw.com.stationmanager.Listener.IScanListener
import tracmo.tw.com.stationmanager.R
import tracmo.tw.com.stationmanager.T4JobService.Companion.isConnect
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity() {

    private val TAG: String = this.javaClass.simpleName

    private var message: TextView? = null
    private var fab: FloatingActionButton? = null
    private var fab1: FloatingActionButton? = null
    private var fab2: FloatingActionButton? = null

    private val listener = object:IScanListener {
        override fun result(info: T4Info) {
            super.result(info)
            when (info.isT4 && !info.macAddress.isNullOrEmpty()) {
                true -> {
                    //set wifi data while scan T4 success
                    LogD(TAG, "scan T4 success")
                    Jobs(applicationContext).scheduleJobs(CONNECT, null)
                    ScanCallback(applicationContext).onScan(this, false)
                }
                false -> fabVisibility(false)
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CallbackMessageEvent){
        when(event.msg){
            getString(R.string.ssid_list_index) -> startActivity(Intent(this, WifiListActivity::class.java))
            getString(R.string.disconnect) -> {
//                when(message!!.visibility){
//                    View.INVISIBLE -> {
//                        recyclerView.visibility = View.INVISIBLE
//                        message!!.visibility = View.VISIBLE
//                        title = getString(R.string.app_name)
//                    }
//                }
                info.disconnect()
//                when(pullToRefresh!!.isEnabled){ true -> pullToRefresh!!.isEnabled = false }
                //todo, hide list and cmd section
                message!!.append("\n")
                message!!.append(event.msg)
                CallbackMessageEvent("")
                //restart scan
                ScanCallback(applicationContext).onScan(listener, true)
//            }
//            getString(R.string.start_connect) -> {
//                scheduleJobs(SCAN)
//                when(message!!.visibility){
//                    View.INVISIBLE -> {
//                        recyclerView.visibility = View.INVISIBLE
//                        message!!.visibility = View.VISIBLE
//                        title = getString(R.string.app_name)
//                    }
//                }

//                when(pullToRefresh!!.isEnabled){ true -> pullToRefresh!!.isEnabled = false }
//                message!!.append("\n")
//                message!!.append(event.msg)
            }
            getString(R.string.connect) -> {
                finish()
                startActivity(intent)
            }
            else -> {
                message!!.append("\n")
                message!!.append(event.msg)
                CallbackMessageEvent("")
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {

        when(info.isT4){
            true -> {
                fabVisibility(true)
                menu.findItem(R.id.disconnect).isVisible = true
            }
            false -> {
                fabVisibility(false)
                menu.findItem(R.id.disconnect).isVisible = false
            }
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.disconnect -> {
                when(info.isT4){
                    true -> {
                        info.disconnect()
                        finish()
                        startActivity(intent)
                    }
                }

                true
            }
            else -> super.onOptionsItemSelected(item);
        }

    }

    override fun onBackPressed() { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val windowWid = Resources.getSystem().displayMetrics.widthPixels

        LogD(TAG, "${EnableBLE(WeakReference(this)).isBLEEnable()} : ${EnableLocation(WeakReference(this)).isLocationEnabled()}")
        when(EnableBLE(WeakReference(this)).isBLEEnable() &&
                EnableLocation(WeakReference(this)).isLocationEnabled()){
            false -> {
                val intent = Intent(this.applicationContext, PermissionActivity::class.java)
                startActivity(intent)
            }
            true -> EnableBLE(WeakReference(this)).enableBLE()
        }

        message = findViewById(R.id.receive_message)
        message!!.append(getString(R.string.log))

        fab = findViewById(R.id.fab)
        fab1 = findViewById(R.id.fab1)
        fab2 = findViewById(R.id.fab2)

    }

    override fun onResume() {
        super.onResume()
        when (!EventBus.getDefault().isRegistered(this)) {
            true -> EventBus.getDefault().register(this@MainActivity)
        }
        //start scan
        when(info.isT4){
            false -> ScanCallback(applicationContext).onScan(listener, true)
            true -> fabVisibility(true)
        }
    }


    override fun onStop() {
        //stop scan
        ScanCallback(applicationContext).onScan(listener, false)
        EventBus.getDefault().unregister(this@MainActivity)
        super.onStop()
    }

    private fun fabVisibility(visible: Boolean){

        LogD(TAG, "$visible")
        when(visible){
            false -> {
                fab!!.visibility = View.INVISIBLE
                fab1!!.visibility = View.INVISIBLE
                fab2!!.visibility = View.INVISIBLE
            }
            true -> {
                fab!!.visibility = View.VISIBLE
                fab1!!.visibility = View.VISIBLE
                fab2!!.visibility = View.VISIBLE
            }
        }

    }

    private var isFABOpen = false

    private fun showFABMenu() {
        isFABOpen = true
        fab1!!.animate().translationY(-resources.getDimension(R.dimen.standard_55))
        fab2!!.animate().translationY(-resources.getDimension(R.dimen.standard_105))
    }

    private fun closeFABMenu() {
        isFABOpen = false
        fab1!!.animate().translationY(0f)
        fab2!!.animate().translationY(0f)
    }

    fun onClick(v: View){

        when(v.id){
            R.id.fab -> {
                when(isFABOpen){
                    true -> closeFABMenu()
                    false -> showFABMenu()
                }
            }
            R.id.fab1 -> { startActivity(Intent(this, WifiListActivity::class.java)) }
            R.id.fab2 -> { startActivity(Intent(this, CmdActivity::class.java)) }
        }
    }


}
