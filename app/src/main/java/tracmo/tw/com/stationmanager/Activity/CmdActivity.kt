/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.View
import android.widget.TextView
import info
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import tracmo.tw.com.stationmanager.Entity.Logger
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Event.CMDEvent
import tracmo.tw.com.stationmanager.Event.CallbackMessageEvent
import tracmo.tw.com.stationmanager.Jobs
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DEVICE_ADDRESS
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DISCONNECT_AND_STOP_ADVERTISING
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_DNS_CONFIG
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_FCC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_FIRMWARE_VERSION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_HARDWARE_VERSION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_IC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_INTERNET_STATUS
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_JRF_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_BLINK_ONCE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_FLASH_ON
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_OFF
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_LED_ON
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_MANUFACTURER_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_MODEL_INDORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_NCC_INFORMATION
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_NETWORK_MODE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_OTA_STATUS
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_RESTART_DEVICE
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_SSID_CONFIG
import tracmo.tw.com.stationmanager.Jobs.Companion.COMMAND_WIFI_STATUS
import tracmo.tw.com.stationmanager.ListAdapter
import tracmo.tw.com.stationmanager.ListAdapter.Companion.CMD
import tracmo.tw.com.stationmanager.R

class CmdActivity: AppCompatActivity(){

    private val TAG = this.javaClass.simpleName
    private var recyclerView: RecyclerView? = null
    private val cmdMap = HashMap<Int, String>()
    private var tvText: TextView? = null

    //request status
    private val OTA_STATUS = "OTA Status"
    private val WIFI_STATUS = "WIFI Status"
    private val INTERNET_STATUS = "Internet Status"
    private val RESTART_DEVICE = "Restart Device"
    private val DISCONNECT_AND_STOP_ADVERTISING = "Disconnect And Stop Advertising"
    private val LED_OFF = "LED Off"
    private val LED_ON = "LED_On"
    private val LED_FLASH_ON = "LED Flash On"
    private val LED_BLINK_ONCE = "LED Blink Once"
    private val NETWORK_MODE = "Network Mode"
    private val DNS_CONFIG = "DNS Config"
    private val SSID_CONFIG = "SSID Config"
    //request information
    private val MANUFACTURER_INFORMATION = "Manufacturer Info"
    private val DEVICE_ADDRESS = "Device Address"
    private val MODEL_INFORMATION = "Model Info"
    private val HARDWARE_VERSION = "Hardware Version"
    private val FIRMWARE_VERSION = "Firmware Version"
    private val FCC_INFORMATION = "FCC Info"
    private val IC_INFORMATION = "IC Info"
    private val NCC_INFORMATION = "NCC Info"
    private val JRF_INFORMATION = "JRF Info"


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CMDEvent){
        Logger.LogD(TAG, "onMessageEvent SetWifiEvent")
        tvText!!.text = ""
        when(event.msg){
            OTA_STATUS -> Jobs(applicationContext).scheduleJobs(COMMAND_OTA_STATUS, null)
            WIFI_STATUS -> Jobs(applicationContext).scheduleJobs(COMMAND_WIFI_STATUS, null)
            INTERNET_STATUS -> Jobs(applicationContext).scheduleJobs(COMMAND_INTERNET_STATUS, null)
            RESTART_DEVICE -> Jobs(applicationContext).scheduleJobs(COMMAND_RESTART_DEVICE, null)
            DISCONNECT_AND_STOP_ADVERTISING -> Jobs(applicationContext).scheduleJobs(COMMAND_DISCONNECT_AND_STOP_ADVERTISING, null)
            LED_OFF -> Jobs(applicationContext).scheduleJobs(COMMAND_LED_OFF, null)
            LED_ON -> Jobs(applicationContext).scheduleJobs(COMMAND_LED_ON, null)
            LED_FLASH_ON -> Jobs(applicationContext).scheduleJobs(COMMAND_LED_FLASH_ON, null)
            LED_BLINK_ONCE ->  Jobs(applicationContext).scheduleJobs(COMMAND_LED_BLINK_ONCE, null)
            NETWORK_MODE -> Jobs(applicationContext).scheduleJobs(COMMAND_NETWORK_MODE, null)
            DNS_CONFIG -> Jobs(applicationContext).scheduleJobs(COMMAND_DNS_CONFIG, null)
            SSID_CONFIG -> Jobs(applicationContext).scheduleJobs(COMMAND_SSID_CONFIG, null)
            MANUFACTURER_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_MANUFACTURER_INFORMATION, null)
            DEVICE_ADDRESS -> Jobs(applicationContext).scheduleJobs(COMMAND_DEVICE_ADDRESS, null)
            MODEL_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_MODEL_INDORMATION, null)
            HARDWARE_VERSION -> Jobs(applicationContext).scheduleJobs(COMMAND_HARDWARE_VERSION, null)
            FIRMWARE_VERSION -> Jobs(applicationContext).scheduleJobs(COMMAND_FIRMWARE_VERSION, null)
            FCC_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_FCC_INFORMATION, null)
            IC_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_IC_INFORMATION, null)
            NCC_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_NCC_INFORMATION, null)
            JRF_INFORMATION -> Jobs(applicationContext).scheduleJobs(COMMAND_JRF_INFORMATION, null)
        }
        tvText!!.text = getString(R.string.sending)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CallbackMessageEvent){

        when{
            event.msg.contains("Scan") || event.msg.contains("Request") -> return
            event.msg == getString(R.string.disconnect) -> {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }

        LogD(TAG, "${event.msg} CallbackMessageEvent")
        tvText!!.text = event.msg

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cmd)

        when(cmdMap.isEmpty()){
            true -> {
                cmdMap[0] = OTA_STATUS
                cmdMap[1] = WIFI_STATUS
                cmdMap[2] = INTERNET_STATUS
                cmdMap[3] = RESTART_DEVICE
                cmdMap[4] = DISCONNECT_AND_STOP_ADVERTISING
                cmdMap[5] = LED_OFF
                cmdMap[6] = LED_ON
                cmdMap[7] = LED_FLASH_ON
                cmdMap[8] = LED_BLINK_ONCE
                cmdMap[9] = NETWORK_MODE
                cmdMap[10] = DNS_CONFIG
                cmdMap[11] = SSID_CONFIG
                cmdMap[12] = MANUFACTURER_INFORMATION
                cmdMap[13] = DEVICE_ADDRESS
                cmdMap[14] = MODEL_INFORMATION
                cmdMap[15] = HARDWARE_VERSION
                cmdMap[16] = FIRMWARE_VERSION
                cmdMap[17] = FCC_INFORMATION
                cmdMap[18] = IC_INFORMATION
                cmdMap[19] = NCC_INFORMATION
                cmdMap[20] = JRF_INFORMATION
            }
        }

        recyclerView = findViewById(R.id.cmd_list)
        recyclerView!!.layoutManager = LinearLayoutManager(this)

        tvText = findViewById(R.id.callback_message)
        tvText!!.setBackgroundColor(this.resources.getColor(R.color.highlight_color))

    }

    override fun onResume() {
        super.onResume()
        when (!EventBus.getDefault().isRegistered(this)) {
            true -> EventBus.getDefault().register(this@CmdActivity)
        }
        recyclerView!!.adapter = ListAdapter(cmdMap, CMD)

    }

    override fun onStop() {
        EventBus.getDefault().unregister(this@CmdActivity)
        super.onStop()
    }

}