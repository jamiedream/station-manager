/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import android.app.Activity
import android.content.Context
import android.net.wifi.WifiManager
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import java.lang.ref.WeakReference

class EnableWifi(activityWeak: WeakReference<Activity>){

    private val TAG: String = this.javaClass.simpleName
    private var wifiManager: WifiManager = activityWeak.get()!!.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    fun switchWifi(status: Boolean){
        LogD(TAG, "switchWifi " + status)
        wifiManager.setWifiEnabled(status)
    }

    fun isWifiEnable(): Boolean{
        return wifiManager.isWifiEnabled
    }

}