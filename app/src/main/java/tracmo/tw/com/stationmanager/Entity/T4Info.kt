/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.content.Context
import info
import tracmo.tw.com.stationmanager.Callback.GattCallback
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogE

class T4Info{

    private val TAG = this.javaClass.simpleName
    var isT4 = false
    var macAddress: String? = null
    lateinit var localName: String
    lateinit var mm: String
    var gattCallback: GattCallback? = null
    var gatt: BluetoothGatt? = null

    /**
     * Every connect action do here!
     * If [.gattCallback] null, means this callback first create, may be first add
     * or service restart.
     *
     * [@boolean#autoConnect][BluetoothDevice.connectGatt]
     * true: hold connect util the device is available then connect
     * false: return callback right away, got disconnected if not available
     * Set false first time, true to others
     *
     * @param callback callback handler
     * @param context in this design, it will be a service
     * @return true if gatt object is successfully allocated (does not mean it is connected),
     * otherwise false
     */
    fun connect(callback: GattCallback?, context: Context): Boolean {

        when(macAddress.isNullOrEmpty()){ true -> return false }
        val device: BluetoothDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(macAddress)

        when (gattCallback == null) {
        // if gatt has been allocated previously, close it and get another one
            false -> {
                when (gatt == null) {
                    false -> {
                        gatt!!.disconnect()
                        gatt!!.close()
                        gatt = null
                    }
                }
                gattCallback = null

                Logger.LogW(TAG, "re-connect to gatt ${info.macAddress}")

                gattCallback = callback
                gatt = device.connectGatt(context, true, gattCallback)
            }

            true -> {
                Logger.LogW(TAG, "connect to gatt ${info.macAddress}")
                gattCallback = callback
                // sony restart phone use true will not success?
                gatt = device.connectGatt(context, false, gattCallback)

            }

        }

        return gatt != null
    }

    fun disconnect(){

        LogE(TAG, "disconnect")
        isT4 = false
        macAddress = null
        when (gatt != null) {
            true -> {
                gatt!!.disconnect()
                gatt!!.close()
                gatt = null
            }
        }
    }

}