/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import android.util.Log
import tracmo.tw.com.stationmanager.BuildConfig

class Logger{

    companion object {

        fun LogD(TAG: String, msg: String){
            if(BuildConfig.DEBUG)
                Log.d(TAG, msg)
        }

        fun LogE(TAG: String, msg: String){
            if(BuildConfig.DEBUG)
                Log.e(TAG, msg)
        }

        fun LogV(TAG: String, msg: String){
            if(BuildConfig.DEBUG)
                Log.v(TAG, msg)
        }

        fun LogI(TAG: String, msg: String){
            if(BuildConfig.DEBUG)
                Log.i(TAG, msg)
        }

        fun LogW(TAG: String, msg: String){
            if(BuildConfig.DEBUG)
                Log.w(TAG, msg)
        }
    }

}