/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import tracmo.tw.com.stationmanager.Entity.EnableBLE
import tracmo.tw.com.stationmanager.Entity.EnableLocation
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.ListAdapter
import tracmo.tw.com.stationmanager.ListAdapter.Companion.PERMISSION
import tracmo.tw.com.stationmanager.Listener.IPermission
import tracmo.tw.com.stationmanager.R
import tracmo.tw.com.stationmanager.Receiver
import java.lang.ref.WeakReference

class PermissionActivity: AppCompatActivity(), IPermission {

    private val TAG: String = this.javaClass.simpleName
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission)

        Receiver(this)
        recyclerView = findViewById(R.id.list)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ListAdapter(permissionList(), this, PERMISSION)

        LogD(TAG, "PermissionActivity")

    }

    fun refreshView(){

        Thread.sleep(2000)
        recyclerView.adapter = ListAdapter(permissionList(), this, PERMISSION)
        recyclerView.adapter.notifyDataSetChanged()

        LogD(TAG, "${EnableBLE(WeakReference(this)).isBLEEnable()}")
        LogD(TAG, "${EnableLocation(WeakReference(this)).isLocationEnabled()}")
        when(EnableBLE(WeakReference(this)).isBLEEnable() &&
                EnableLocation(WeakReference(this)).isLocationEnabled()){
            true -> startActivity(Intent(applicationContext, MainActivity::class.java))
        }
    }

    override fun onBackPressed() { }



    override fun result(position: Int, status: Boolean) {
        super.result(position, status)
        LogD(TAG, "$position")
        when(position){
            0 -> EnableBLE(WeakReference(this)).enableBLE()
            1 -> EnableLocation(WeakReference(this)).enableLocation()
        }

        refreshView()
    }

    private fun permissionList(): HashMap<String, Boolean>{
        val permissionList = HashMap<String, Boolean>()
        permissionList.put(getString(R.string.ble_permission), EnableBLE(WeakReference(this)).isBLEEnable())
        permissionList.put(getString(R.string.location_permission), EnableLocation(WeakReference(this)).isLocationEnabled())
//        LogD(TAG, "PermissionActivity : ${EnableBLE(WeakReference(this)).isBLEEnable()} : ${getString(R.string.ble_permission)}")
//        LogD(TAG, "PermissionActivity : ${EnableLocation(WeakReference(this)).isLocationEnabled()} : ${getString(R.string.location_permission)}")

        return permissionList
    }


}




