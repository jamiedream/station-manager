/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Entity

import tracmo.tw.com.stationmanager.Jobs
import tracmo.tw.com.stationmanager.Jobs.Companion.DNS_1
import tracmo.tw.com.stationmanager.Jobs.Companion.DNS_2
import tracmo.tw.com.stationmanager.Jobs.Companion.INDEX
import tracmo.tw.com.stationmanager.Jobs.Companion.IP_ADDRESS
import tracmo.tw.com.stationmanager.Jobs.Companion.NAME
import tracmo.tw.com.stationmanager.Jobs.Companion.PASSWORD
import tracmo.tw.com.stationmanager.Jobs.Companion.ROUTER
import tracmo.tw.com.stationmanager.Jobs.Companion.SUBNET_MASK

class StaticIP(private val index: String?, val name: String?, val password: String,
               val ipAddress: String, val subnetMask: String, val router: String, val dns1: String, val dns2: String){

    private val TAG = this.javaClass.simpleName
    fun mapData(): HashMap<String, String>{
        val map = HashMap<String, String>()
        map[INDEX] = index!!
        map[PASSWORD] = password
        map[IP_ADDRESS] = ipAddress
        map[SUBNET_MASK] = subnetMask
        map[ROUTER] = router
        map[DNS_1] = dns1
        map[DNS_2] = dns2
        Logger.LogD(TAG, index)
        Logger.LogD(TAG, password)
        Logger.LogD(TAG, ipAddress)
        Logger.LogD(TAG, subnetMask)
        Logger.LogD(TAG, router)
        Logger.LogD(TAG, dns1)
        Logger.LogD(TAG, dns2)
        return map
    }

    fun mapNewData(): HashMap<String, String>{
        val map = HashMap<String, String>()
        map[NAME] = name!!
        map[PASSWORD] = password
        map[IP_ADDRESS] = ipAddress
        map[SUBNET_MASK] = subnetMask
        map[ROUTER] = router
        map[DNS_1] = dns1
        map[DNS_2] = dns2
        Logger.LogD(TAG, name)
        Logger.LogD(TAG, password)
        Logger.LogD(TAG, ipAddress)
        Logger.LogD(TAG, subnetMask)
        Logger.LogD(TAG, router)
        Logger.LogD(TAG, dns1)
        Logger.LogD(TAG, dns2)
        return map
    }

}
