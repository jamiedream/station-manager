/*
 * ******************************************************************************
 *  * Copyright (c) 2018
 *  * Tracmo, Inc. ("Tracmo").
 *  * All rights reserved. *
 *  * The information contained herein is confidential and proprietary to
 *  * Tracmo. Use of this information by anyone other than authorized employees
 *  * of Tracmo is granted only under a written non-disclosure agreement,
 *  * expressly prescribing the scope and manner of such use. *
 *  * Code may contain thrid party SDK. Refer to the license.txt with
 *  * the project for the information of the third party license and/or
 *  * its registered trademarks. *
 *  *
 *  * Created by J.Y. 9/11/2018
 *  *****************************************************************************
 */

package tracmo.tw.com.stationmanager.Callback

import ADVERTISING_TRACMO_COMPANY_ID
import ADVERTISING_TRACMO_HEADER
import ADVERTISING_TRACMO_NAME
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothAdapter.getDefaultAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Build
import info
import org.greenrobot.eventbus.EventBus
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogD
import tracmo.tw.com.stationmanager.Entity.Logger.Companion.LogV
import tracmo.tw.com.stationmanager.Entity.T4Info
import tracmo.tw.com.stationmanager.Event.CallbackMessageEvent
import tracmo.tw.com.stationmanager.Listener.IScanListener
import tracmo.tw.com.stationmanager.R
import java.nio.ByteBuffer
import java.util.*

class ScanCallback(context: Context){

    private val TAG = this.javaClass.simpleName
    private val c = context

    fun onScan(listener: IScanListener, status: Boolean){

        val sdk = Build.VERSION.SDK_INT
        LogD(TAG, "onScan $status")
        when{
            sdk < 21 -> {
                LogD(TAG, "sdk < 21 $status")
//                device, rssi, scanRecord -> parseAdvertising(device, scanRecord, listener)
                val callback = object: BluetoothAdapter.LeScanCallback {
                    override fun onLeScan(device: BluetoothDevice?, rssi: Int, scanRecord: ByteArray?) {
                        when(status){
                            true -> when(info.isT4) { false -> parseAdvertising(device!!, scanRecord!!, listener) }
                        }
                    }
                }
                when(status){
                    true -> {
                        getDefaultAdapter().startLeScan(callback)
                        EventBus.getDefault().post(CallbackMessageEvent(c.getString(R.string.start_scan)))
                    }
                    false -> {
                        getDefaultAdapter().stopLeScan(callback)
                        EventBus.getDefault().post(CallbackMessageEvent(c.getString(R.string.stop_scan)))
                    }
                }
            }
            else -> {
                LogD(TAG, "sdk >= 21 $status")
                val callback = object :ScanCallback() {
                    override fun onScanResult(callbackType: Int, result: ScanResult?) {
                        super.onScanResult(callbackType, result)
//                        LogD(TAG, "$result.device")
                        when(status){
                            true -> when(info.isT4) { false -> parseAdvertising(result!!.device, result.scanRecord!!.bytes, listener)}
                        }
                    }}
                val scanner = BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
                when(scanner == null){
                    false -> {
                        when (status) {
                            true -> {
                                scanner.startScan(callback)
                                EventBus.getDefault().post(CallbackMessageEvent(c.getString(R.string.start_scan)))
                            }
                            false -> {
                                scanner.stopScan(callback)
                                EventBus.getDefault().post(CallbackMessageEvent(c.getString(R.string.stop_scan)))
                            }
                        }
                    }
                }
            }
        }

    }

    fun parseAdvertising(device: BluetoothDevice, bleData: ByteArray, listener: IScanListener){

        val info = parseBleData(device, bleData)
        if (info.isT4 && info.macAddress != null) {
//            LogV(TAG, "${info.isT4} ${info.macAddress.isNullOrBlank()} success")
            listener.result(info)
        }
    }

    private fun parseBleData(device: BluetoothDevice, advData: ByteArray): T4Info {

//        val info = T4Info()
        val buffer = ByteBuffer.wrap(advData)
        //advData.size == 62
        when(advData.size > 61) {
            true -> {
                //read byte array
                when (Arrays.equals(byteArrayOf(buffer.get(0)), ADVERTISING_TRACMO_HEADER) &&
                        Arrays.equals(byteArrayOf(buffer.get(5), buffer.get(6), buffer.get(7), buffer.get(8)), ADVERTISING_TRACMO_COMPANY_ID)) {
                    true -> {
                        info.isT4 = true
                        LogV(TAG, "${info.isT4} info.isT4")
                    }
                }

                when(Arrays.equals(byteArrayOf(buffer.get(19)), ADVERTISING_TRACMO_NAME)) {
                    true -> {
                        val nameOffset = 20
                        buffer.position(nameOffset)
                        readT4Name(info, buffer, device)
                    }
                }
            }
        }

        return info
    }

    private fun readT4Name(info: T4Info, buffer: ByteBuffer, device: BluetoothDevice){

        val length = buffer.get(18).toInt()
        LogV(TAG, "$length length")
        when(length != 21){ true -> return }
        val sb = ByteArray(length)
        buffer.get(sb, 0, length)
        info.localName = String(sb).trim { it <= ' ' }
        when (info.localName.isBlank()) {
            false -> {
                when {
                    info.localName.startsWith(c.getString(R.string.scan_name)) -> {
                        info.mm = info.localName.substring(8, 20)
                        LogV(TAG, info.mm)
                    }
                }
                info.macAddress = device.address
                LogV(TAG, "${info.macAddress} addr")
                LogV(TAG, "${info.localName} local name")
            }

        }
    }

}